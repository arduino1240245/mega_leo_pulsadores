//Hecho para el MEGA 2560

//Pin 22 ==> pulsador1 con pull up
//Pin 24 ==> pulsador2 con pull up
//Pin 26 ==> led1
//Pin 28 ==> led2

//Apreto pulsador1 ==> prende led1
//Apreto pulsador2 ==> prende led2

#define PULSADOR1 22
#define PULSADOR2 24
#define LED1      26
#define LED2      28

int pulsador1 = 0, pulsador2 = 0;

void setup() {
  // put your setup code here, to run once:

  pinMode (PULSADOR1, INPUT);    //Pulsador1
  pinMode (PULSADOR2, INPUT);    //Pulsador2
  pinMode (LED1, OUTPUT);   //Led1
  pinMode (LED2, OUTPUT);   //Led2
}

void loop() {
  // put your main code here, to run repeatedly:
  
  pulsador1 = digitalRead(PULSADOR1);
  pulsador2 = digitalRead(PULSADOR2);
 

  if (pulsador1 == HIGH) digitalWrite (LED1, HIGH);
  else digitalWrite (LED1, LOW);

  if (pulsador2 == HIGH) digitalWrite (LED2, HIGH);
  else digitalWrite (LED2, LOW);
  
}
